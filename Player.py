import pygame


class Player:
    def __init__(self, x, y, width, height, win):
        self.x = x
        self.y = y
        self.width = width
        self.height = height
        self.color = (255, 255, 255)
        self.win = win
        self.vel = 5
        self.points = 0
        self.Collide = None

    def draw(self):
        self.Collide = pygame.draw.rect(self.win, self.color, (self.x, self.y, self.width, self.height))

    def score(self):
        self.points += 1


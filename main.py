import pygame
from Player import Player
from Ball import Ball

# Initiiere Pygme Umgebung
pygame.init()

# Aktiviere Screen
winHeight = 600
winWidth = 1000
win = pygame.display.set_mode((winWidth, winHeight))

# Farben
orange = (255, 140, 0)
rot = (255, 0, 0)
gruen = (0, 255, 0)
schwarz = (0, 0, 0)
weiss = (255, 255, 255)

# solange wie True läuft das Spiel
spielAktiv = True

# Bildschirmaktualisierung
clock = pygame.time.Clock()
FPS = 60

# Schleife Hauptprogramm
pygame.display.set_caption("PingPong")

# Score
font = pygame.font.SysFont("comicsans", 200, True)

# Player
barHeight = 100
barWidth = 10
Player1 = Player(20, (winHeight / 2) - (barHeight / 2), barWidth, barHeight, win)
Player2 = Player(winWidth - 30, (winHeight / 2) - (barHeight / 2), barWidth, barHeight, win)

# Spielball
Ball = Ball(int(winWidth / 2) - 5, int(winHeight / 2) - 5, 10, barWidth, barHeight, win)
leftBound = Ball.radius / 2
rightBound = winWidth - Ball.radius / 2

while spielAktiv:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            spielAktiv = False
            print("Spieler hat Quit-Button angeklickt")

    win.fill(schwarz)
    Player1.draw()
    Player2.draw()
    Ball.draw()
    text = font.render(str(Player1.points) + " : " + str(Player2.points), 1, weiss)
    win.blit(text, (325, 20))

    # Spiellogik
    keys = pygame.key.get_pressed()  # Listet gedrückte Tasten auf

    # Spieler2 Schläger Bewegung
    if keys[pygame.K_UP] and Player2.y > Player2.vel:
        Player2.y -= Player2.vel

    if keys[pygame.K_DOWN] and Player2.y < winHeight - Player2.vel - Player2.height:
        Player2.y += Player2.vel

    # Spieler1 Schläger Bewegung
    if keys[pygame.K_w] and Player1.y > Player1.vel:
        Player1.y -= Player1.vel

    if keys[pygame.K_s] and Player1.y < winHeight - Player1.vel - Player1.height:
        Player1.y += Player1.vel

    # Leertaste gedrückt
    if keys[pygame.K_SPACE]:
        Ball.aktiv = True

    if Ball.aktiv:
        Ball.move()

    if Player1.Collide.colliderect(Ball.Collide) or \
            Player2.Collide.colliderect(Ball.Collide):
        Ball.dirX = Ball.dirX * -1

    if Ball.x >= rightBound:
        Ball.point(Player1)
        print("Spieler 1 hat ein Punkt erzielt")

    elif Ball.x <= leftBound:
        Ball.point(Player2)
        print("Spieler 2 hat ein Punkt erzielt")

    # Spielfeld und -figuren zeichnen

    pygame.display.update()

    # Refresh festlegen
    clock.tick(FPS)
pygame.quit()

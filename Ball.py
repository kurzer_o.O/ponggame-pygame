import pygame
import random


class Ball:

    def __init__(self, x, y, radius, barWidth, barHeight, win):
        self.x = x
        self.y = y
        self.xstart = x
        self.ystart = y
        self.radius = radius
        self.speed = 15
        self.win = win
        self.color = (255, 0, 0)
        self.barW = barWidth
        self.barH = barHeight
        self.screenWidth, self.screenHeight = pygame.display.get_surface().get_size()
        self.dirX = 0.7
        self.dirY = 0.3
        self.aktiv = False
        self.Collide = None

    def draw(self):
        self.Collide = pygame.draw.circle(self.win, self.color, (self.x, self.y), self.radius)

    def move(self):

        upperBound = self.radius / 2
        lowerBound = self.screenHeight - self.radius / 2

        if self.aktiv:
            self.x = self.x + int(self.dirX * self.speed)
            self.y = self.y + int(self.dirY * self.speed)
            self.draw()
        else:
            self.start()

        if self.y <= upperBound or self.y >= lowerBound:
            self.dirY = self.dirY * -1

    def point(self, player):
        player.score()
        self.start()
        self.aktiv = False

    def start(self):
        pygame.draw.circle(self.win, self.color, (self.xstart, self.ystart), self.radius)
        self.dirX = round(random.uniform(.5, .7), 1)
        self.dirY = 1 - self.dirX
        self.aktiv = True
        self.x = self.xstart
        self.y = self.ystart
